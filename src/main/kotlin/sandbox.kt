package com.example.sandbox

/**
 * Целочисленное сложение
 * @param a1 Первый аргумент
 * @param a2 Второй аргумент
 * @return Сумма
 */
fun add(a1: Int, a2: Int) = a1 + a2
